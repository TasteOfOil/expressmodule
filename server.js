const express = require('express');
const cors = require('cors');
const Joi = require('joi');
const app = express();

app.use(express.json());
app.use(cors({
    origin:'*'
}));

const port = process.env.PORT || 3000;
app.listen(port, ()=>{
    console.log(`Listening on PORT: ${port}`)
})

const students = [
    {id: 1, name: "Kiril", age:18},
    {id: 2, name: "Rob", age:21},
    {id: 3, name: "Lily", age:17},
    {id: 4, name: "Sveta", age:24}
];

app.get('/',(req,res)=>{
    console.log("Homepage");
    res.send('Hello! You are on a homepage;)');
});

app.get('/api/students',(req,res)=>{
    console.log("GET students");
    res.send(students);
});

app.get('/api/students/:id',(req,res)=>{
    const student = students.find((el, index, array)=>{
        return el.id === parseInt(req.params.id);
    }); 
    if(!student)return res.status(404).send(`Student (ID:${req.params.id}) not found`);
    console.log(`GET students/${req.params.id}`);
    res.send(student);
});

app.post('/api/students',(req,res)=>{
    const schema = Joi.object({
        name: Joi.string().required(),
        age: Joi.number().required()
    });
    const result = schema.validate(req.body);
    if(result.error){
        res.status(400).send('Bad Request');
        return;
    }

    const student = {
        id: students.length + 1,
        name: req.body.name,
        age: req.body.age
    };
    students.push(student);
    console.log(`POST students/${student.id}`);
    res.send(student);
});

app.put('/api/students/:id', (req,res)=>{
    const student = students.find((el, index, array)=>{
        return el.id === parseInt(req.params.id);
    });
    if(!student){
        res.status(404).send(`Student (ID:${req.params.id}) not found`);
        return;
    }
    
    const schema = Joi.object({
        name: Joi.string().required(),
        age: Joi.number().required()
    });
    const result = schema.validate(req.body);
    if(result.error){
        res.status(400).send('Bad Request');
        return;
    }

    student.name = req.body.name;
    student.age = parseInt(req.body.age);
    console.log(`PUT students/${student.id}`);
    res.send(student);

});

app.delete('/api/students/:id', (req,res)=>{
    const student = students.find((el, index, array)=>{
        return el.id === parseInt(req.params.id);
    });
    if(!student){
        res.status(404).send(`Student (ID:${req.params.id}) not found`);
        return;
    }

    const idx = students.indexOf(student);
    students.splice(idx, 1);
    console.log(`DELETE students/${student.id}`);
    res.send(student);

})